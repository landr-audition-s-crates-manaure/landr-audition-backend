<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $contacts = Contact::all();
            return response()->json($contacts, 200);
        } catch (\Throwable $th) {
            $error = 'Error on getting contact list';
            return response()->json($error, 500);
        }
    }    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $contact = new Contact();
            $contact->name = $request->name;
            $contact->job_title = $request->job_title;
            $contact->address = $request->address;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->save();
            return response()->json($contact, 200);
        } catch (\Throwable $th) {
            $error = 'Error on creating contact';
            return response()->json($error, 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show(Contact $contact)
    {
        return response()->json($contact, 200);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        try {
            $contact->name = $request->name;
            $contact->job_title = $request->job_title;
            $contact->address = $request->address;
            $contact->phone = $request->phone;
            $contact->email = $request->email;
            $contact->save();
            return response()->json($contact, 200);
        } catch (\Throwable $th) {
            $error = 'Error on updating contact';
            return response()->json($error, 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        try {
            $contact->delete();
            return response()->json('Contact deleted', 200);
        } catch (\Throwable $th) {
            $error = 'Error on updating contact';
            return response()->json($error, 500);
        }
    }
}
