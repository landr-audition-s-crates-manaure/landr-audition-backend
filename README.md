## Pre-requisities

1. **PHP version: 7.3.11**
2. **Composer version: 2.0.2**
3. **MariaDB version: 10.4.8**

## Execute project

1. Create database for project
2. Generate .env file from .env.example and fill up database variables.
3. Execute **composer install**
4. Execute **php artisan migrate:refresh**
5. Execute **php artisan serve**
